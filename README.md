# Content moderation reviewer

The module allows to assign people to review a piece of content.

## Usage instructions

* Install the content moderation reviewer module
* Create your workflow using the workflow and content\_moderation module.
* Ensure to grant permissions to the various transitions in your workflow.
* When an author creates new content they can choose people to be assigned as reviewer.
* The reviewer must be able to execute at least one transition from the state the entity is transitioning to.

## Site builder usage

* When you create views you can choose to add a relationship to the moderation reviewer
* This allows you to filter by content the current user is assigned to etc.

In general it might be worth checking out the example in the cmr\_test module.
This adds a workflow, associated roles as well as some test views.

### Running tests

* To run the tests use [phpunit](https://www.drupal.org/docs/automated-testing/phpunit-in-drupal/running-phpunit-tests)
