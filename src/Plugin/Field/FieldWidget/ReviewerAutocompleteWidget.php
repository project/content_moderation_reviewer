<?php

declare(strict_types = 1);

namespace Drupal\content_moderation_reviewer\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\content_moderation\ContentModerationState;
use Drupal\content_moderation\ModerationInformation;
use Drupal\content_moderation_reviewer\CmrConstants;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'entity_reference_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = \Drupal\content_moderation_reviewer\Plugin\Field\FieldWidget\ReviewerAutocompleteWidget::PLUGIN_ID,
 *   label = @Translation("Reviewer Autocomplete"),
 *   description = @Translation("An autocomplete text field."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ReviewerAutocompleteWidget extends WidgetBase {

  public const PLUGIN_ID = 'content_moderation_reviewer_autocomplete';

  /**
   * The ajax wrapper id for the field.
   */
  protected static function ajaxWrapperId(string $field_name): string {
    return $field_name . '-content-moderation-reviewer';
  }

  /**
   * Gets the moderation information service.
   */
  protected function getModerationInformation(): ModerationInformation {
    return \Drupal::service('content_moderation.moderation_information');
  }

  /**
   * Get the to_state, can come from multiple sources.
   *
   * E.g, when change the state and ajax fires, the to state will be in user
   * input.
   */
  public static function getToStateId(array $form, FormStateInterface $form_state): string {
    if (!empty($form_state->getUserInput()['moderation_state'][0]['state'])) {
      return $form_state->getUserInput()['moderation_state'][0]['state'];
    }
    elseif ($form_state->hasValue(['moderation_state', 0, 'state'])) {
      return $form_state->getValue(['moderation_state', 0, 'state']);
    }

    // Fallback to the first state in the moderation_state dropdown.
    return key($form['moderation_state']['widget'][0]['state']['#options']);
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    assert($items instanceof EntityReferenceFieldItemListInterface);
    $build = ['target_id' => []];
    $formObject = $form_state->getFormObject();
    if (!$formObject instanceof EntityFormInterface) {
      return $build;
    }
    $entity = $formObject->getEntity();
    assert($entity instanceof ContentEntityInterface);

    if ($workflow = $this->getModerationInformation()->getWorkflowForEntity($entity)) {
      $from_state = $entity->moderation_state->value;
      $to_state = $this->getToStateId($form, $form_state);

      $form['moderation_state']['#attributes']['id'] = static::ajaxWrapperId(CmrConstants::FIELD_NAME);

      $build['target_id']['#type'] = 'value';
      $build['target_id']['#element_validate'] = [[__CLASS__, 'elementValidate']];
      $form['#process'][] = [__CLASS__, 'process'];

      // Don't allow to set a reviewable once content is published.
      $to_state_object = $workflow->getTypePlugin()->getState($to_state);
      assert($to_state_object instanceof ContentModerationState);
      if ($to_state_object->isPublishedState()) {
        return $build;
      }

      $build['target_id']['#type'] = 'textfield';
      $build['target_id']['#title'] = $element['#title'];
      $build['target_id']['#description'] = $element['#description'];

      $referenced_entities = $items->referencedEntities();
      // Only set default value when states match. This is mostly for when
      // editing an existing entity which only has a single transition from its
      // from_state. We want to force users to select a new reviewer in that
      // case.
      if ($from_state === $to_state) {
        $build['target_id']['#default_value'] = isset($referenced_entities[$delta]) ? static::userToAutocompleteLabel($referenced_entities[$delta]) : '';
      }

      $build['target_id']['#autocomplete_route_name'] = 'content_moderation_reviewer.autocomplete';
      $build['target_id']['#autocomplete_route_parameters'] = [
        'workflow_id' => $workflow->id(),
        'to_state' => $to_state,
      ];
    }

    return $build;
  }

  /**
   * Resets the content_moderation_reviewer on ajax requests.
   */
  public static function elementValidate(&$element, FormStateInterface $form_state, &$form): void {
    // Don't manipulate the form data when there is a real submission going on.
    if ($form_state->isSubmitted()) {
      return;
    }

    $entity = $form_state->getFormObject()->getEntity();
    assert($entity instanceof ContentEntityInterface);
    $from_state_id = $entity->get('moderation_state')->value;
    $to_state_id = $form_state->getValue(['moderation_state', 0, 'value']);
    if ($from_state_id !== $to_state_id) {
      NestedArray::setValue($form_state->getUserInput(), $element['#parents'], '');
      NestedArray::setValue($form_state->getValues(), $element['#parents'], '');
    }
  }

  /**
   * Format the autocomplete label.
   */
  public static function userToAutocompleteLabel(UserInterface $user): string {
    return sprintf('%s (%s)', $user->label(), $user->id());
  }

  /**
   * Process the element.
   *
   * Adds AJAX wrappers and moves field.
   */
  public static function process(array $element): array {
    $element['moderation_state'][CmrConstants::FIELD_NAME] = $element[CmrConstants::FIELD_NAME];

    $element['moderation_state']['widget'][0]['state']['#ajax'] = [
      'callback' => [__CLASS__, 'updateWidget'],
      'wrapper' => static::ajaxWrapperId(CmrConstants::FIELD_NAME),
    ];

    unset($element[CmrConstants::FIELD_NAME]);

    return $element;
  }

  /**
   * AJAX callback.
   */
  public static function updateWidget($element, FormStateInterface $form_state): array {
    return $element['moderation_state'];
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return parent::isApplicable($field_definition) && $field_definition->getFieldStorageDefinition()->getName() === CmrConstants::FIELD_NAME;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $error, array $form, FormStateInterface $form_state) {
    return $element['target_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $key => $value) {
      $values[$key]['target_id'] = EntityAutocomplete::extractEntityIdFromAutocompleteInput($value['target_id']);
    }

    return $values;
  }

}
