<?php

declare(strict_types = 1);

namespace Drupal\content_moderation_reviewer\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Verifies that the set reviewer is valid.
 *
 * @Constraint(
 *   id = "ModerationStateReviewer",
 *   label = @Translation("Valid moderation reviewer", context = "Validation")
 * )
 */
class ModerationStateReviewerConstraint extends Constraint {

  public string $message = 'Invalid moderation reviewer from %from to %to';

}
