<?php

declare(strict_types = 1);

namespace Drupal\content_moderation_reviewer\Plugin\Validation\Constraint;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation_reviewer\AccessChecker;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Checks if a moderation reviewer is valid.
 */
class ModerationStateReviewerConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Creates a new ModerationStateConstraintValidator instance.
   */
  public function __construct(
    protected ModerationInformationInterface $moderationInformation,
    protected AccessChecker $accessChecker,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('content_moderation.moderation_information'),
      $container->get('content_moderation_reviewer.access_checker'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $entity = $value->getEntity();
    assert($entity instanceof ContentEntityInterface);
    assert($constraint instanceof ModerationStateReviewerConstraint);
    assert($value instanceof EntityReferenceFieldItemListInterface);

    // Ignore entities that are not subject to moderation anyway.
    if (!$this->moderationInformation->isModeratedEntity($entity)) {
      return;
    }

    if ($entity->isNew()) {
      return;
    }

    if ($value->isEmpty()) {
      return;
    }

    $workflow = $this->moderationInformation->getWorkflowForEntity($entity);

    // If a new state is being set and there is an existing state, validate
    // there is a valid transition between them.
    $new_state = $workflow->getTypePlugin()->getState($entity->moderation_state->value);
    $original_state = $this->moderationInformation->getOriginalState($entity);
    $entities = $value->referencedEntities();
    $reviewer = reset($entities);

    if ($reviewer instanceof UserInterface && !$this->accessChecker->isValidReviewer($workflow->id(), $reviewer, $new_state->id())) {
      $this->context->addViolation($constraint->message, [
        '%uid' => $reviewer->id(),
        '%from' => $original_state->label(),
        '%to' => $new_state->label(),
      ]);
    }
  }

}
