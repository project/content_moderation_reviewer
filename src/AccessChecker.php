<?php

declare(strict_types = 1);

namespace Drupal\content_moderation_reviewer;

use Drupal\content_moderation\ContentModerationState;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;
use Drupal\workflows\TransitionInterface;
use Drupal\workflows\WorkflowInterface;

/**
 * Provides a way for code to filter users by transitions they are allowed to.
 */
class AccessChecker {

  /**
   * Constructs a new AccessChecker.
   */
  public function __construct(protected EntityTypeManagerInterface $entityTypeManager) {
  }

  /**
   * Return users which can transition beyond the to_state.
   *
   * When chosen a given state ID, this function returns role IDs which can
   * execute at least 1 transition from $to_state_id.
   */
  public function roleIdsWithAllowedTransition(string $workflow_id, string $to_state_id): array {
    $workflow = $this->entityTypeManager->getStorage('workflow')->load($workflow_id);
    if (!$workflow instanceof WorkflowInterface) {
      return [];
    }
    $transitions = $workflow->getTypePlugin()
      ->getTransitionsForState($to_state_id);
    $permissions = array_map(function (TransitionInterface $transition) use ($workflow) {
      return "use {$workflow->id()} transition {$transition->id()}";
    }, $transitions);

    // Sadly config query doesn't support array values yet, so we have to
    // load all roles manually and filter them by permission.
    $role_storage = $this->entityTypeManager->getStorage('user_role');
    $rids = array_map(function (RoleInterface $role) use ($permissions) {
      if ($role->isAdmin()) {
        return $role->id();
      }
      foreach ($permissions as $permission) {
        if ($role->hasPermission($permission)) {
          return $role->id();
        }
      }
      return NULL;
    }, $role_storage->loadMultiple());
    return array_filter($rids);
  }

  /**
   * Validates whether a user is a valid reviewer for the given to_state_id.
   */
  public function isValidReviewer(string $workflow_id, UserInterface $reviewer, $to_state_id): bool {
    assert(is_string($workflow_id));
    $workflow = $this->entityTypeManager->getStorage('workflow')->load($workflow_id);
    assert($workflow instanceof WorkflowInterface);

    $to_state = $workflow->getTypePlugin()->getState($to_state_id);
    assert($to_state instanceof ContentModerationState);

    // You cannot assign a reviewer for actually published content.
    if ($to_state->isPublishedState()) {
      return FALSE;
    }

    $reviewer_role_ids = $reviewer->getRoles();
    $allowed_roles = $this->roleIdsWithAllowedTransition($workflow_id, $to_state_id);
    return !empty(array_intersect($reviewer_role_ids, $allowed_roles));
  }

}
