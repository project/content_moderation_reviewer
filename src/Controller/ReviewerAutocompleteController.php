<?php

declare(strict_types = 1);

namespace Drupal\content_moderation_reviewer\Controller;

use Drupal\content_moderation_reviewer\AccessChecker;
use Drupal\content_moderation_reviewer\Plugin\Field\FieldWidget\ReviewerAutocompleteWidget;
use Drupal\Core\Controller\ControllerBase;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides autocompletion for the reviewer based upon current workflow state.
 */
class ReviewerAutocompleteController extends ControllerBase {

  /**
   * Controller constructor.
   */
  public function __construct(protected AccessChecker $accessChecker) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('content_moderation_reviewer.access_checker'),
    );
  }

  /**
   * Returns a list of users who can transition from $to_state.
   */
  public function __invoke(Request $request, string $workflow_id, string $to_state): JsonResponse {
    $roleIds = $this->accessChecker->roleIdsWithAllowedTransition($workflow_id, $to_state);

    $string = $request->query->get('q');
    $userStorage = $this->entityTypeManager()->getStorage('user');
    $users = [];
    if ($roleIds) {
      $ids = $userStorage->getQuery()
        ->condition('roles', $roleIds, 'IN')
        ->condition('name', $string, 'CONTAINS')
        ->pager(0, 10)
        ->accessCheck(FALSE)
        ->execute();
      $users = $userStorage->loadMultiple($ids);
    }

    return new JsonResponse(array_values(array_map(function (UserInterface $user) {
      return [
        'value' => ReviewerAutocompleteWidget::userToAutocompleteLabel($user),
        'label' => $user->label(),
      ];
    }, $users)));
  }

}
