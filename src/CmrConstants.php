<?php

declare(strict_types = 1);

namespace Drupal\content_moderation_reviewer;

/**
 * Defines constants for CMR.
 */
final class CmrConstants {

  public const FIELD_NAME = 'content_moderation_reviewer';

}
