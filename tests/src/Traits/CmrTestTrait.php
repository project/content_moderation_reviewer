<?php

declare(strict_types = 1);

namespace Drupal\Tests\content_moderation_reviewer\Traits;

/**
 * Test helpers for CMR.
 */
trait CmrTestTrait {

  /**
   * @var \Drupal\Core\Session\AccountInterface[]
   */
  protected array $users;

  /**
   * Loads test users from cmr_test_install into class prop.
   */
  public function loadTestUsers(): void {
    $user_names = [
      'test drafter 1',
      'test drafter 2',
      'test editorial_reviewer 1',
      'test editorial_reviewer 2',
      'test legal_reviewer 1',
      'test legal_reviewer 2',
    ];
    $user_storage = \Drupal::entityTypeManager()->getStorage('user');
    foreach ($user_names as $name) {
      $users = $user_storage->loadMultiple($user_storage->getQuery()->accessCheck(FALSE)->condition('name', $name)->execute());
      $this->users[$name] = reset($users);
    }
  }

}
