<?php

declare(strict_types = 1);

namespace Drupal\Tests\content_moderation_reviewer\FunctionalJavascript;

use Behat\Mink\Element\NodeElement;
use Drupal\content_moderation_reviewer\CmrConstants;
use Drupal\content_moderation_reviewer\Plugin\Field\FieldWidget\ReviewerAutocompleteWidget;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\content_moderation_reviewer\Traits\CmrTestTrait;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests the output of entity reference autocomplete widgets.
 *
 * @group entity_reference
 */
class ReviewerAutocompleteWidgetTest extends WebDriverTestBase {

  use ContentTypeCreationTrait;
  use NodeCreationTrait;
  use CmrTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'cmr_test',
    'content_moderation',
    'workflows',
    'system',
    'node',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Install our module late so the field is correctly added, otherwise
    // content_moderation_reviewer_entity_base_field_info will bail early for
    // Node and the field won't exist.
    \Drupal::service('module_installer')->install(['content_moderation_reviewer']);
    $this->loadTestUsers();
  }

  /**
   * Tests the autocomplete functionality for draft/editorial_review states.
   *
   * For some reason logging in twice in WebDriver tests fails so we have to
   * split this up by role.
   */
  public function testReviewerAutocompleteWidgetDraftEditorialReview(): void {
    $this->drupalLogin($this->drupalCreateUser(values: [
      'roles' => ['test_drafter'],
    ]));
    $this->drupalGet('node/add/page');

    // Ensure description is rendered.
    $this->assertSession()->elementTextContains('css', '.form-item-content-moderation-reviewer-0-target-id .description', 'Select a reviewer');

    // Page is in draft, all users should be shown.
    // (users provisioned in cmr_test_install().
    $results = $this->doAutocomplete();
    $this->assertCount(6, $results);

    // Changing to editorial_review should show editorial reviewers only.
    $this->doChangeModerationState('editorial_review');
    $results = $this->doAutocomplete();
    $this->assertCount(2, $results);
    $this->assertEquals([
      'test editorial_reviewer 1',
      'test editorial_reviewer 2',
    ], $results);
  }

  /**
   * Tests the autocomplete functionality for legal_review state.
   */
  public function testReviewerAutocompleteWidgetLegalReview(): void {
    $this->drupalLogin($this->drupalCreateUser(values: [
      'roles' => ['test_editorial_reviewer'],
    ]));
    $node = $this->createNode([
      'moderation_state' => 'editorial_review',
    ]);
    $this->drupalGet($node->toUrl('edit-form'));
    $this->doChangeModerationState('legal_review');
    $results = $this->doAutocomplete();
    $this->assertCount(2, $results);
    $this->assertEquals([
      'test legal_reviewer 1',
      'test legal_reviewer 2',
    ], $results);
  }

  /**
   * Tests the autocomplete functionality for legal_review state.
   */
  public function testReviewerAutocompleteWidgetPublished(): void {
    $this->drupalLogin($this->drupalCreateUser(values: [
      'roles' => ['test_legal_reviewer'],
    ]));
    $node = $this->createNode([
      'moderation_state' => 'legal_review',
    ]);
    $this->drupalGet($node->toUrl('edit-form'));
    $this->doChangeModerationState('published');
    // No reviewer field for transitioning to published.
    $this->assertSession()->elementNotExists('css', '[name="content_moderation_reviewer[0][target_id]"]');
    $this->submitForm([], 'Save');

    // From published -> draft should default to draft from state with all
    // users selectable.
    $this->drupalGet($node->toUrl('edit-form'));
    $results = $this->doAutocomplete();
    $this->assertCount(6, $results);
  }

  /**
   * Tests the default value for the field widget.
   */
  public function testReviewerAutocompleteWidgetDefaultValue(): void {
    $this->drupalLogin($this->drupalCreateUser(values: [
      'roles' => ['test_editorial_reviewer'],
    ]));
    $reviewer = $this->drupalCreateUser(values: [
      'roles' => ['test_editorial_reviewer'],
    ]);
    $node = $this->createNode([
      'moderation_state' => 'editorial_review',
      CmrConstants::FIELD_NAME => $reviewer,
    ]);
    $this->drupalGet($node->toUrl('edit-form'));
    $this->assertSession()->fieldValueEquals('moderation_state[0][state]', 'editorial_review');
    $this->assertSession()->fieldValueEquals('content_moderation_reviewer[0][target_id]', ReviewerAutocompleteWidget::userToAutocompleteLabel($reviewer));
    // Remove editorial_review_keep transition so we can test default values
    // between different states without testing the published state as that
    // hides the field.
    $this->doChangeModerationState('legal_review');
    // Changing states clears the value.
    $this->assertSession()->fieldValueEquals('content_moderation_reviewer[0][target_id]', '');
    $workflowConfig = \Drupal::configFactory()->getEditable('workflows.workflow.test_workflow');
    $transitions = $workflowConfig->get('type_settings.transitions');
    unset($transitions['editorial_review_keep']);
    $workflowConfig->set('type_settings.transitions', $transitions);
    $workflowConfig->save();

    $this->drupalGet($node->toUrl('edit-form'));
    $this->assertSession()->fieldValueEquals('moderation_state[0][state]', 'legal_review');
    $this->assertSession()->fieldValueEquals('content_moderation_reviewer[0][target_id]', '');
  }

  /**
   * Executes an autocomplete on a given field and waits for it to finish.
   *
   * @return array
   *   The array of autocomplete results.
   */
  protected function doAutocomplete(): array {
    $autocomplete_field = $this->assertSession()->waitForElement('css', '[name="content_moderation_reviewer[0][target_id]"].ui-autocomplete-input');
    $autocomplete_field->setValue('test');
    $this->getSession()->getDriver()->keyDown($autocomplete_field->getXpath(), ' ');
    $this->assertSession()->waitOnAutocomplete();
    return array_map(function (NodeElement $element) {
      return $element->getText();
    }, $this->getSession()->getPage()->findAll('css', '.ui-autocomplete li'));
  }

  /**
   * Changes the moderation state field and waits for the ajax request.
   */
  protected function doChangeModerationState(string $state): void {
    $moderationStateField = $this->getSession()->getPage()->findField('moderation_state[0][state]');
    $moderationStateField->setValue($state);
    $this->assertSession()->assertWaitOnAjaxRequest();
  }

}
