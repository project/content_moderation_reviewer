<?php

declare(strict_types = 1);

namespace Drupal\Tests\content_moderation_reviewer\Kernel;

/**
 * Tests the access checker.
 *
 * @group content_moderation_reviewer
 * @coversDefaultClass \Drupal\content_moderation_reviewer\AccessChecker
 */
class AccessCheckerTest extends CmrTestBase {

  /**
   * @covers ::roleIdsWithAllowedTransition
   */
  public function testRoleIdsWithAllowedTransition(): void {
    /** @var \Drupal\content_moderation_reviewer\AccessChecker $accessChecker */
    $accessChecker = \Drupal::service('content_moderation_reviewer.access_checker');

    $statesRoles = [
      'draft' => [
        'test_drafter',
        'test_editorial_reviewer',
        'test_legal_reviewer',
      ],
      'editorial_review' => [
        'test_editorial_reviewer',
      ],
      'legal_review' => [
        'test_legal_reviewer',
      ],
      'published' => [
        'test_drafter',
        'test_editorial_reviewer',
        'test_legal_reviewer',
      ],
    ];
    foreach ($statesRoles as $state => $roles) {
      $this->assertEqualsCanonicalizing($roles, array_values($accessChecker->roleIdsWithAllowedTransition('test_workflow', $state)));
    }
  }

}
