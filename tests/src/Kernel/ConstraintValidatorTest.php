<?php

declare(strict_types = 1);

namespace Drupal\Tests\content_moderation_reviewer\Kernel;

use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * @see \Drupal\content_moderation_reviewer\Plugin\Validation\Constraint\ModerationStateReviewerConstraintValidator
 *
 * @group content_moderation_reviewer
 */
class ConstraintValidatorTest extends CmrTestBase {

  /**
   * Tests the constraint.
   */
  public function testValidTransition(): void {
    $node = Node::create([
      'type' => 'page',
      'title' => 'Test title',
    ]);
    $node->moderation_state->value = 'draft';
    $node->save();
    assert($node instanceof NodeInterface);

    // Test Draft -> Editorial Review.
    $to_state_id = 'editorial_review';
    $data_provider = [
      'draft->editorial review user:test drafter 1' => [$this->users['test drafter 1'], FALSE],
      'draft->editorial review user:test drafter 2' => [$this->users['test drafter 2'], FALSE],
      'draft->editorial review user:test editorial_reviewer 1' => [$this->users['test editorial_reviewer 1'], TRUE],
      'draft->editorial review user:test editorial_reviewer 2' => [$this->users['test editorial_reviewer 2'], TRUE],
      'draft->editorial review user:test legal_reviewer 1' => [$this->users['test legal_reviewer 1'], FALSE],
      'draft->editorial review user:test legal_reviewer 2' => [$this->users['test legal_reviewer 2'], FALSE],
    ];

    $node->moderation_state->value = $to_state_id;
    foreach ($data_provider as $key => [$user, $valid]) {
      $node->set('content_moderation_reviewer', $user->id());

      if ($valid) {
        $this->assertEmpty($node->validate(), $key);
      }
      else {
        $this->assertNotEmpty($node->validate(), $key);
      }
    }

    // Test Editorial Review -> Legal Review.
    // Switch accounts to avoid transition validation errors.
    \Drupal::service('account_switcher')->switchTo($this->users['test editorial_reviewer 1']);
    $node->set('content_moderation_reviewer', NULL);
    $node->moderation_state->value = 'editorial_review';
    $node->save();
    $node = Node::load($node->id());

    $to_state_id = 'legal_review';
    $data_provider = [
      'editorial->legal_review review user:test drafter 1' => [$this->users['test drafter 1'], FALSE],
      'editorial->legal_review review user:test drafter 2' => [$this->users['test drafter 2'], FALSE],
      'editorial->legal_review review user:test editorial_reviewer 1' => [$this->users['test editorial_reviewer 1'], FALSE],
      'editorial->legal_review review user:test editorial_reviewer 2' => [$this->users['test editorial_reviewer 2'], FALSE],
      'editorial->legal_review review user:test legal_reviewer 1' => [$this->users['test legal_reviewer 1'], TRUE],
      'editorial->legal_review review user:test legal_reviewer 2' => [$this->users['test legal_reviewer 2'], TRUE],
    ];

    $node->get('moderation_state')->value = $to_state_id;
    foreach ($data_provider as $key => [$user, $valid]) {
      $node->set('content_moderation_reviewer', $user->id());

      if ($valid) {
        $this->assertEmpty($node->validate(), $key);
      }
      else {
        $this->assertNotEmpty($node->validate(), $key);
      }
    }

    // Test Legal Review -> Published.
    \Drupal::service('account_switcher')->switchTo($this->users['test legal_reviewer 1']);
    $node->set('content_moderation_reviewer', NULL);
    $node->get('moderation_state')->value = 'legal_review';
    $node->save();
    $node = Node::load($node->id());

    $to_state_id = 'published';
    $data_provider = [
      'editorial->legal_review review user:test drafter 1' => [$this->users['test drafter 1'], FALSE],
      'editorial->legal_review review user:test drafter 2' => [$this->users['test drafter 2'], FALSE],
      'editorial->legal_review review user:test editorial_reviewer 1' => [$this->users['test editorial_reviewer 1'], FALSE],
      'editorial->legal_review review user:test editorial_reviewer 2' => [$this->users['test editorial_reviewer 2'], FALSE],
      'editorial->legal_review review user:test legal_reviewer 1' => [$this->users['test legal_reviewer 1'], FALSE],
      'editorial->legal_review review user:test legal_reviewer 2' => [$this->users['test legal_reviewer 2'], FALSE],
    ];

    $node->get('moderation_state')->value = $to_state_id;
    foreach ($data_provider as $key => [$user, $valid]) {
      $node->get('content_moderation_reviewer')->target_id = $user->id();

      if ($valid) {
        $this->assertEmpty($node->validate(), $key);
      }
      else {
        $this->assertNotEmpty($node->validate(), $key);
      }
    }
  }

}
