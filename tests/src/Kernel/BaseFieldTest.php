<?php

declare(strict_types = 1);

namespace Drupal\Tests\content_moderation_reviewer\Kernel;

use Drupal\content_moderation_reviewer\CmrConstants;

/**
 * Tests the base field.
 *
 * @group content_moderation_reviewer
 */
final class BaseFieldTest extends CmrTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
  ];

  /**
   * Test the base field is only added to moderated entity types.
   */
  public function testBaseField(): void {
    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager */
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $this->assertArrayHasKey(CmrConstants::FIELD_NAME, $entityFieldManager->getBaseFieldDefinitions('node'));
    $this->assertArrayNotHasKey(CmrConstants::FIELD_NAME, $entityFieldManager->getBaseFieldDefinitions('entity_test'));
  }

}
