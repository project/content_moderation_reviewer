<?php

declare(strict_types = 1);

namespace Drupal\Tests\content_moderation_reviewer\Kernel;

use Drupal\content_moderation_reviewer\Plugin\Field\FieldWidget\ReviewerAutocompleteWidget;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * @see \Drupal\content_moderation_reviewer\Controller::reviewerAutocomplete
 *
 * @group content_moderation_reviewer
 */
class AutocompletionTest extends CmrTestBase {

  /**
   * Tests the autocomplete endpoint.
   */
  public function testValidTransition(): void {
    $this->assertReviewerAutocompleteResponse('draft', [
      'test drafter 1',
      'test drafter 2',
      'test editorial_reviewer 1',
      'test editorial_reviewer 2',
      'test legal_reviewer 1',
      'test legal_reviewer 2',
    ]);

    $this->assertReviewerAutocompleteResponse('editorial_review', [
      'test editorial_reviewer 1',
      'test editorial_reviewer 2',
    ]);

    $this->assertReviewerAutocompleteResponse('legal_review', [
      'test legal_reviewer 1',
      'test legal_reviewer 2',
    ]);

    $this->assertReviewerAutocompleteResponse('published', [
      'test drafter 1',
      'test drafter 2',
      'test editorial_reviewer 1',
      'test editorial_reviewer 2',
      'test legal_reviewer 1',
      'test legal_reviewer 2',
    ]);
  }

  /**
   * Assert the response contains the expected list of users for a given state.
   */
  protected function assertReviewerAutocompleteResponse(string $fromState, array $userList): void {
    $http_kernel = \Drupal::service('http_kernel');
    assert($http_kernel instanceof HttpKernelInterface);
    $response = $http_kernel->handle(Request::create('/content_moderation_reviewer/test_workflow/' . $fromState, 'GET', ['q' => 'test']));
    $this->assertEquals(200, $response->getStatusCode());
    $this->assertJson($response->getContent());
    $userList = array_map(function (string $userName) {
      return [
        'value' => ReviewerAutocompleteWidget::userToAutocompleteLabel($this->users[$userName]),
        'label' => $userName,
      ];
    }, $userList);
    $this->assertEquals($userList, json_decode($response->getContent(), TRUE));
  }

}
