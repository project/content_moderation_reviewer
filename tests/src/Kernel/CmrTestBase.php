<?php

declare(strict_types = 1);

namespace Drupal\Tests\content_moderation_reviewer\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\content_moderation_reviewer\Traits\CmrTestTrait;
use Drupal\user\Entity\User;

/**
 * Test base for CMR kernel tests.
 */
abstract class CmrTestBase extends KernelTestBase {

  use CmrTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation_reviewer',
    'content_moderation',
    'workflows',
    'system',
    'node',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('content_moderation_state');
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');

    $this->installSchema('system', 'sequences');
    $this->installSchema('node', 'node_access');

    // Ensure we have an admin user so cmr_test_install() doesn't create admin
    // users.
    User::create([
      'name' => 'admin',
    ])->save();

    \Drupal::service('module_installer')->install(['cmr_test']);
    $this->loadTestUsers();
    \Drupal::service('account_switcher')->switchTo($this->users['test drafter 1']);
  }

}
